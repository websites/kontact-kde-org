---
aliases:
  - /kontact
layout: component
title: Kontact
screenshot: 'https://cdn.kde.org/screenshots/kontact/kontact.png'
shortDescription: >
    Handle all your emails, calendars and contacts within a single window.
description: >
    Kontact unifies all our PIM applications into a single window to give you
    the best possible experience and boost your productivity. With Kontact you
    can use all the tools of our powerful suite in one place. Unlike proprietary
    web applications we value your privacy and support open standards, which is
    why Kontact gives you full control over your data. You can of course also
    access your data offline if you want to.
weight: 10
---

Kontact is a feature-rich personal information management
suite created by [KDE](https://kde.org). Kontact unifies all our PIM
applications into a single window so that you can deal with your emails,
calendaring, news and other data even more efficiently. You can also run each
of the components of Kontact as a standalone application.

## Feature-rich
We believe that software should adapt to your workflows, instead of forcing
you to adapt to theirs. Kontact is highly configurable and customizable so that
you can set it up exactly the way that works for you. Check out the individual
pages for Kontact's components to get an overview of some of the features each
of them can offer.

## Flexible
Kontact supports a large variety of email, calendaring and addressbook services
and protocols and can be easily extended to support additional services. You
don't need to change your email provider or migrate your calendar elsewhere just
to get it synced into Kontact. Just point Kontact at the service where your PIM
data are and Kontact will display it and keep it synchronized.

Kontact can also import your PIM data and some settings from other PIM suites
like Thunderbird and Evolution.

## Secure
Kontact supports all modern security standards to protect your privacy. All
communication with remote servers is encrypted by default using the industry
standard SSL/TLS encryption. Kontact also has a built-in support for signing
and encryption emails with GPG and S/MIME and it can automatically verify emails
signatures.

Kontact is configured by default to protect your privacy by blocking loading
external content, automatic responses and other tricks often used by scammers.

## Integrated
All components of Kontact are well integrated together. KOrganizer can send
meeting invitations through email accounts configured in KMail. KMail will
render meeting invitations and show you conflicts with events in your
calendars. You can mix your Todos and regular events in KOrganizer's Agenda
view.

## Free and Open Source
Kontact is free (both as in free speech and free beer) and completely open
source. It means that the [source code](https://invent.kde.org/pim/kontact) is
out there for anyone to see, audit
and improve.

We pride ourselves in our openness and our focus on protecting
users' privacy. Kontact will not spy on you, won't show any advertisement, it
will not store your sensitive data somewhere in a proprietary cloud or share
them with anyone else. See the
[KDE Software Privacy Policy](https://kde.org/privacypolicy-apps/) for more
details.
