---
aliases:
  - /kaddressbook
layout: component
title: KAddressBook
screenshot: 'https://cdn.kde.org/screenshots/kaddressbook/kaddressbook.png'
shortDescription: Manage your contacts easily.
description: >
    KAddressBook allows you to manage your contacts easily. It is deeply
    integrated with the rest of Kontact, allowing you to easily pick your
    contacts when sending an email or creating a meeting invitation.
weight: 30
---

KAddressBook stores all the personal details of your family, friends and
other contacts. It supports large variety of services, including NextCloud,
Kolab, Google Contacts, Microsoft Exchange (EWS) or any standard CalDAV
server.

## Features
* **Powerful search.** KAddresbook has configurable filters and powerful
  search capabilities.
* **Duplicate detection and merging.** KAdressbook can find duplicated contacts
  from multiple sources can merge them into a single contact.
* **Integration.** Integrates with other Kontact components, e.g. exporting
  Birthday reminders to [KOrganizer](../korganizer).
* **QR codes.** KAddressbook can display a QR code for each contact to quickly
  scan it into your phone or send to someone.
* **Good standard support.** KAddresbook can import and export contacts from
  and to nearly every address book standard.
* **LDAP integration.** Can connect to multiple LDAP servers, which can then
  be used for contact autocompletion when composing emails in
  [KMail](../kmail).

