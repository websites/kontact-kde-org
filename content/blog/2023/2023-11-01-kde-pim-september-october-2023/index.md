---
title: September and October in KDE PIM
author: Carl Schwan
date: 2023-11-01T13:00:00+00:00
---

Here's our bi-monthly update from KDE's personal information management
applications team. This report covers progress made in the months of September and
October 2023.

Since [the last report](/blog/2023/2023-09-01-kde-pim-july-august-2023/), 35
people contributed approximately 1600 code changes, focusing on bugfixes and
improvements for the coming 24.02 release based on Qt6.

## Transition to Qt6/KDE Frameworks 6

As we plan for KDE Gear 24.02 to be our first Qt6-based release, there's been
on-going work on more Qt6 porting, removing deprecated functions, removing support
for the qmake build system, and so on.

## KMail

Email notifications let you open specific messages or folders directly for each
individual emails or groups of emails in the notification history.

![New mail notification history showing an open in folder or display email for each email](newmailnotificationhistory.png)

Laurent also worked on an improved adblock feature in the email viewer based on
the code of AngelFish and Brave (it uses Rust!).

There has also been some small memory usage improvements as we load some widgets on
demand now.

## KTextAddons

*KTextAddons* is the library that provides many of the text specific utilities in KDE PIM and
in other applications.

Laurent is hard at work adding support for the local translation engine
[Bergamot](https://browser.mt/), as the developers are working on converting
*Bergamot* to a more reusable library.

There has been also some improvements around speech to text support, in
particular with the support of the [vosk-api](https://github.com/alphacep/vosk-api)
engine, an offline speech recognition API.

## KAlarm

David Jarvie fixed the *Show Today* button not showing up in the date picker when
using the Breeze icon theme. The icons now install to the correct location.

When an alarm is deferred, we now ensure that it is deleted from the archived
calendar.

Additionally, we carried out various small fixes and improvements on the user interface.

## KOrganizer

After previously implementing [initial support for sending cryptographically signed or
encrypted event invitations](https://volkerkrause.eu/2023/07/05/kde-pim-may-june-2023.html#korganizer),
Dan has now completed the feature by adding checkboxes to allow the event organizer to
configure whether the invitation should be sent signed and/or encrypted.

![](incidenceeditor-privacy.png)

There are further plans to improve the overall attendees editor to share more 
code and features with the recipient lines in KMail's message composer. This 
will further improve the crypto-related UX.

Another visible improvement is that the iCal Akonadi resource configuration 
dialogs now explain more clearly that it accepts both local files and remote 
URLs, and has new controls to configure the periodic refresh of the calendar. This 
is especially useful for remote iCal calendars.

![Before](singlefileresource-before.png)

![After](singlefileresource-after.png)

We fixed email invitations to all-day events being off by one day compared to 
the original event ([#421400](https://bugs.kde.org/show_bug.cgi?id=421400),
and fixed a bug that caused counter-proposals to meeting invitations being invalid
and ignored by other calendaring applications.

We solved two crashes, one that occured when you clicked the *Apply* button when
creating a new event with attendees ([#428888](https://bugs.kde.org/show_bug.cgi?id=428888)),
and the other that made KOrganizer crash when forwarding and event via the context
menu ([#474144](https://bugs.kde.org/show_bug.cgi?id=474144)).

We fixed some layout and scrollbar issues in the side-by-side agenda view, and
improved the performance of the event list view.

Dan's work on improving KOrganizer was funded by [g10 Code](https://g10code.com).

## Kirigami Addons

In *Kirigami Addons*, there is now a Date and Time picker which can be used in
*Merkuro*, *Itinerary* and other Kirigami applications.

![Date picker](datepicker.png)

## Merkuro

Merkuro was ported to Qt6 and we fixed most of the regressions caused by the port. We also 
ported all the list delegates of Merkuro to the new *RoundedItemDelegate* and
*RoundedTreeDelegate* component. Similarly, our *MobileForm* usage was ported to
*FormCard* and we are now using the *KirigamiAddons.Settings* for the settings dialog.

### Merkuro Calendar

Most of the focus, in the last two months has been on fixing issues in the calendar
module. Carl fixed an off-by-one error in the calendar view when the local
timezone is located west of the UTC timezone (e.g. in the US or in South
America). He also fixed a bug where Merkuro would crash on start up depending on
the locale.

### Merkuro Contact

Merkuro Contact has a redesigned contact page.

![Merkuro contact new contact page](merkuro-contact.png)

Carl added support to edit the birthday and anniversary date, thanks to the
new component of Kirigami Addons.

### Merkuro Mail

Claudio worked on the email account settings and Merkuro now let's you edit
your Identity settings, including the crypto settings. Aakarsh then added
support to edit the incoming accounts settings (IMAP, POP3 and Exchange)

Aakarsh also added support to open *.mbox* and *.eml* files directly with *Merkuro
Mail* and added a context menu to manage the email folders in the sidebar.

## MimeTreeParser

Carl added support for protected headers for both the current version and the
legacy version.

Aside from that, *MimeTreeParser* is now almost fully unit tested and should
handle a lot more edge cases when parsing encrypted and unencrypted emails.

Carl's work on improving *KOrganizer* was funded by [g10 Code](https://g10code.com).

## Itinerary

Itinerary got a new bottom navigation bar, a new journey new UI, more options
to add and edit entries manually and can now share entire trips or individual
entries directly via KDE Connect. For more details see
[Itinerary's own summary blog post](https://volkerkrause.eu/2023/09/29/kde-itinerary-august-september-2023.html).

![KDE itinerary new journey look](kde-itinerary-journey.jpg)
