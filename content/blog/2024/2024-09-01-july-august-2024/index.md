---
title: July and August in KDE PIM
author: KDE PIM Team
date: 2024-09-02T19:20:00+00:00
---

Here's our bi-monthly update from KDE's personal information management
applications team. This report covers progress made in July and August 2024.

Since the last report, 32 people have contributed over 1300 changes to the KDE PIM
code base. We also released a new version of the KDE PIM Suite in August with
the [Gear release](https://kde.org/announcements/gear/24.08.0/)

## Akademy

The KDE PIM team will be at [Akademy from the 7th to the 12th of September in Würzburg
(Germany)](https://akademy.kde.org/2024/). We will host again a [PIM
BoF](https://community.kde.org/Akademy/2024/Monday) on Monday from 14h to 16h.

## Milestones

We have decided to plan and track our work in milestones. Milestones represent
concrete goals with clear definitions of what we understand as done,
and be achievable within a reasonable time frame. Each milestone is then split
into smaller bite-sized tasks that can be worked on independently.

This helps us prioritize important work, make our progress more visible
and, most importantly, make it easier for people to get excited about what we
are working on. New contributors will also be able to pick a well-defined
task and start contributing to PIM.

You can see the milestones on our [Gitlab
board](https://invent.kde.org/groups/pim/-/milestones). If anything there
catches your eye and you would like to help, reach out to us on the
#kontact:kde.org Matrix channel!

### Retiring KJots and KNotes

We made some progress on this front and KNotes was not part of the 24.08
release. The repositories for KNotes and KJots are now archived and the
remaining bits related to the Akonadi Note support were removed from
[KOrganizer](https://invent.kde.org/pim/korganizer/-/merge_requests/123),
[Calendar Support](https://invent.kde.org/pim/calendarsupport/-/merge_requests/67),
[KDE PIM Runtime](https://invent.kde.org/pim/kdepim-runtime/-/merge_requests/177),
and [Event Views](https://invent.kde.org/pim/eventviews/-/merge_requests/104).

### Moving Protocol Implementations to KDE Frameworks

Volker continued to [cleanup and optimize
KMime](https://invent.kde.org/pim/kmime/-/merge_requests?scope=all&state=merged)
in preparation for moving it to the KDE Frameworks. KMime is the library used to
parse and write emails.

## Itinerary

Our travel assistant app [Itinerary](https://apps.kde.org/itinerary) got a new 
seat information display in the timeline, integration with the Träwelling 
check-in service, more use of Wikidata/Wikimedia online content and a pretty 
new [website](https://apps.kde.org/itinerary). 
See [its own bi-monthly update](https://volkerkrause.eu/2024/07/27/kde-itinerary-june-july-2024.html) for 
more details.

## Kleopatra

Over the last two months the smart card views for the different types of supported
smart cards got a facelift to make them look more unified and less crowded
([T7018](https://dev.gnupg.org/T7018)).

![Kleopatra's new OpenPGP smart card view](images/kleopatra-openpgp-card.png)

Kleopatra now supports disabling OpenPGP certificates ([T7216](https://dev.gnupg.org/T7216)).
This is sometimes useful to prevent accidentally using a certificate for encryption.

We improved the usability in
* signing and encryption ([T6485](https://dev.gnupg.org/T6485), [T7183](https://dev.gnupg.org/T7183), [T7236](https://dev.gnupg.org/T7236)),
* the list of certifications that now only shows the relevant ones
([T7231](https://dev.gnupg.org/T7231)),
* the certificate group configuration ([T6966](https://dev.gnupg.org/T6966)),
* changing the expiration of subkeys ([T7198](https://dev.gnupg.org/T7198), [T7215](https://dev.gnupg.org/T7215)).

## Akregator

Akregator is the RSS feed reader integrated into Kontact. Laurent reimplemented
the filter bar to avoid multiple clicks and it is now similar to the one from
Thunderbird.

![Akregator new filter bar](images/akregator-searchbar.png)

Akregator now supports Plasma Activities so you can select which feeds are
visible depending on the activity you are in. Similar functionalities are
planned for KMail, KOrganizer and KAddressBook.

![Akregator Activities settings dialog](images/akregator-activities.png)

Finally, Akregator now has a _What's New_ dialog showing the changes from the
last version.

![Akregator Whats New dialog](images/akregator-whatsnew.png)

## KMail

KMail now uses less memory by only loading some widgets when needed.

## MimeTreeParser/Merkuro

We have unified the verification message for signed messages between Kleopatra,
KMail and Merkuro by moving the implementation to LibKleo.

![Signed message showned in KMail and Merkuro](images/kmail-merkuro.png)

## KAlarm

We replaced the libcanberra audio backend with VLC, since libcanberra is
unmaintained and does not recognise recent audio formats. There is also
the option for distributions to use MPV as audio backend.
