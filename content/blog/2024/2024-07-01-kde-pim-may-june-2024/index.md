---
title: May and June in KDE PIM
author: KDE PIM Team
date: 2024-07-04T09:00:00+00:00
images:
 - "./images/korganizer-nextcloud-tags.png"
---

Here's our bi-monthly update from KDE's personal information management
applications team. This report covers progress made in the months of May and June 2024.

Since [the last report][previous-report] 38 people have contributed over 1500
changes to KDE PIM code base.


## PIM Sprint

Let's start with the biggest event of the last two months: the PIM sprint!

The team met in Toulouse for a weekend of discussions, hacking and French
pastries. You can read reports from [Kevin][kevins-report],
[Carl][carls-report], [Dan][dans-report] and [Volker][volkers-report] on their
blogs to get all the nitty gritty.

In this report, we will cover the biggest topics that were discussed and worked
on during the sprint.

## Milestones

We have decided to plan and track our work in milestones. Milestones should represent a concrete
goal with clear definitions of what we understand as _done_, and be achievable
within a reasonable time frame. Each milestone is then split into smaller
bite-sized tasks that can be worked on independently.

This will help us prioritize important work, make our progress more visible and,
most importantly,  make it easier for people to get excited about what we are
working on. New contributors will also be able to pick up a well-defined task
and start contributing to PIM.

You can see the [milestones on our Gitlab board][milestones-board] - if anything
there catches your eye and you would like to help, reach out to us on the
[#kontact:kde.org Matrix channel][matrix-channel]!

This report, as well as future ones will try to focus on the current milestones
and their progress, hopefully making them more exciting to read :)

## Retiring KJots and KNotes

We have decided to retire the KJots and KNotes applications. These applications
have not seen any support or development in many years and are not in a state
that we feel comfortable shipping to our users. With the introduction of
[Marknote][marknote], KDE can now offer a modern, well-maintained note-taking
application that we can recommend users to migrate to. The latest release of
Marknote has gained support for importing notes from KJots and KNotes, so no
notes will be lost.

## Polished Tag Support

Tags were introduced into KDE PIM many, many years ago, but they have never
reached their full potential. We have decided to change that and make tags a
first-class citizen in our applications. The first step is making sure that tags
are actually usable, so we started by implementing automatic extraction of tags
from events and todos and syncing them into local iCal calendars and remote DAV
calendars. Thanks to this, you can now sync tags between KOrganizer and
NextCloud, for example.

![Tags synchronized between KOrganizer and NextCloud Calendar](./images/korganizer-nextcloud-tags.png)

## Moving Protocol Implementations to KDE Framworks

We have libraries in KDE PIM that implement various standards and protocols.
By moving them to KDE Frameworks we make them independent from KDE PIM and thus
available to anyone who wants to use them. In the past we have moved
KCalendarCore (iCal support library) and KContacts (vCard support library) to
Frameworks. We are now working on moving KMime (email/RFC822 support library)
and KIMAP (IMAP protocol implementations) to Frameworks as well.

This is helping [cleanup KMime][kmime-milestone]. KMime APIs is now in
many places `const` correct to avoid the risk of modifying a message when
reading it, proper CamelCase headers are now generated like for all the KDE
Frameworks. Finally, parsing a MIME file is now up to 10 times faster on typical
emails.

## Other Improvements and Fixes

### Itinerary

Our travel assistant app [Itinerary](https://apps.kde.org/itinerary) gained
support for the public transport routing service
[Transitous](https://transitous.org), got a new import staging area and can now
create new entries directly from [OSM](https://openstreetmap.org) elements. For
more details see its own [summary blog
post](https://volkerkrause.eu/2024/05/30/kde-itinerary-april-may-2024.html).

![Screenshot of Itinerary showing import staging area](./images/kde-itinerary-import-staging-area.png)

### Merkuro

"Snow flurry" fixed the start of the week math for locales that use Sunday as
the first day of the week. They also fixed the navigation of the basic mode for
the month view and week view (which are used on mobile).

Claudio continued working on the Merkuro Mail application and added a progress
bar in the sidebar which appears when a background job is running.

![Merkuro Mail progress status bar](merkuromailprogress.png)

The settings dialogs have been ported to the new
[KirigamiAddons.ConfigurationView][configurationview] which fixes some issues on
mobile.

## Get Involved

If you would like to get involved in KDE PIM, check our [milestones board][milestones-board]
and pick a task! And don't forget to join us in the [#kontact:kde.org Matrix channel][matrix-channel] or the
[kde-pim mailing list](https://mail.kde.org/mailman/listinfo/kde-pim)!

[previous-report]: /blog/2024/2024-05-01-kde-pim-march-april-2024/
[kevins-report]: https://ervin.ipsquad.net/blog/2024/06/16/report-from-kdepim-spring-sprint-2024/
[carls-report]: https://carlschwan.eu/2024/06/16/kde-pim-sprint-2024-edition/
[dans-report]: https://www.dvratil.cz/2024/06/kde-pim-sprint-2024-report/
[volkers-report]: https://volkerkrause.eu/2024/06/22/kde-pim-sprint-june-2024.html
[milestones-board]: https://invent.kde.org/groups/pim/-/milestones
[kmime-milestone]: https://invent.kde.org/pim/pim-technical-roadmap/-/issues/49
[marknote]: https://apps.kde.org/marknote/
[matrix-channel]: https://matrix.to/#/#kontact:kde.org
[configurationview]: https://api.kde.org/kirigami-addons/html/classConfigurationView.html
