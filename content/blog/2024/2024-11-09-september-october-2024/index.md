---
title: September and October in KDE PIM
author: KDE PIM Team
date: 2024-11-09T19:25:00+00:00
---

Here's our bi-monthly update from KDE's personal information management
applications team. This report covers progress made in September and October 2024.

Since the last report, 24 people have contributed over 1100 changes to the KDE PIM
code base. We also released a two bugfix releases of the KDE PIM Suite with
the Gear releases [24.08.1](https://kde.org/announcements/gear/24.08.1/) and
[24.08.2](https://kde.org/announcements/gear/24.08.2/)

Please note this is the last bi-monthly blog post for KDE PIM. We will continue to work
on KDE PIM but weekly improvements to KDE PIM are now included in the
[This Week in KDE Apps](https://blogs.kde.org/categories/this-week-in-kde-apps/) blog.

## Akademy

The KDE PIM team was at [Akademy from the 7th to the 12th of September in Würzburg
(Germany)](https://akademy.kde.org/2024/). We hosted again a [PIM
BoF](https://community.kde.org/Akademy/2024/PIM_BoF).

We covered a few topics and made plans. In particular we touched upon contributions blockers,
we hope the milestone system will help and also working on the amount of repositories which
are not part of KDE Frameworks. Things are progressing in the right direction but slowly.
Feel free to reach out to help!

## Milestones

Talking about the milestones. You can see what we got in store on the [Gitlab
board](https://invent.kde.org/groups/pim/-/milestones). Some of them are progressing nicely
like the resurrection of Kontact for Windows or the port away from QCA.

If you see anything you fancy and you would like to help, reach out to us on the
#kontact:kde.org Matrix channel!

## Applications

### Itinerary

Our travel assistance app Itinerary got a new two-level trip/timeline view, an 
extended public transport location search, a new full trip map view and better 
Android platform integration. Read more in [its own bi-monthly update](https://volkerkrause.eu/2024/10/03/kde-itinerary-august-september-2024.html).

### KAlarm

David has been working on fixing bugs around sound handling. In particular,
[repeating audio alarms only playing once](https://bugs.kde.org/show_bug.cgi?id=492425)
have been fixed. Likewise the [failure to play sound files using libVLC on some
systems](https://bugs.kde.org/show_bug.cgi?id=493128) is gone. Also the backend to
play sound can be changed at build time, it can use VLC or MPV.

But that's not the only bugs which got squashed. It's now possible to
[wake from suspend when using RTC wake](https://bugs.kde.org/show_bug.cgi?id=486187)
and a crash has been fixed affecting systems where the kernel supports alarm timers.

Last but not least, the GUI has been improved around the run mode options in the
preferences dialog.

### Merkuro

Claudio has been busy fixing regressions and improving the stability of 
Merkuro. Notably, maps are now displayed again (if the event contains 
coordinates). Also, the collection combobox in the editors are now initialized with 
a valid collection and filtering features have been repaired.

### KAddressBook and KOrganizer

The general improvements to support Plasma Activities is still on going.
It is not enabled by default as it requires Akonadi Resources support to
become really useful and the corresponding changes are not there yet.

### KMail

On the KMail front the search has been greatly improved. There is now
a custom syntax usable in the search text field. One can now use keywords
like `subject:`, `body:`, `to`, `from`, `has:attachment`, `is:important`,
`is:replied` and so on to make more precise queries.

For instance one could write "`from:vkrause@kde.org to:kde-pim@kde.org is:important`"
to get only the emails from Volker on the kde-pim mailing list which are also flagged
as important.

![KMail Advanced Search](images/kmail-advanced-search.png)

