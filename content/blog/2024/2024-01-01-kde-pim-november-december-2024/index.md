---
title: November and December in KDE PIM
author: KDE PIM Team
date: 2024-01-07T12:00:00+00:00
---

Here's our bi-monthly update from KDE's personal information management
applications team. This report covers progress made in the months of November and December 2023.

Since [the last report](/blog/2023/2023-11-01-kde-pim-september-october-2023/), 35
people contributed approximately 1500 code changes, focusing on bugfixes and
improvements for the coming 24.02 release based on Qt6.

## Transition to Qt6/KDE Frameworks 6

As we plan for KDE Gear 24.02 to be our first Qt6-based release, there's been
on-going work on more Qt6 porting, removing deprecated functions, removing support
for the qmake build system, and so on.

## New feature

Kontact, KMail, KAddressBook and Sieve Editor will now warn you when you are
running an unsuported version of KDE PIM and it is time to update it. The code
for this is based on Kdenlive.

## Itinerary

While the focus for [Itinerary](https://apps.kde.org) was also on completing the transition
to Qt 6, with its nightly Android builds switching over a week ago, there also were a number
of new features added, such as public transport arrival search, a new journey display in the
timeline and a nearby amenity search.
See [Itinerary's bi-monthly status update](https://volkerkrause.eu/2023/12/02/kde-itinerary-october-november-2023.html) for more details.

![Screenshot showing Itinerary's new timline journey view.](images/kde-itinerary-timeline-train-journey-details.png)

## KOrganizer

In KOrganizer it is no possible to search for events without specifying a time range ([BKO#394967](https://bugs.kde.org/394967)).
A new configuration option was added to only notify about events that the user is organizer
or attendee of. This is especially useful when users add a shared calendar from their
colleagues, but don't want to be notified about events that they do not participate in.
Korganizer intercepts SIGINT/SIGTERM signal. It will avoid to lose data during editing.

## Akonadi

Work has begun on creating a tool that would help users to migrate their Akonadi database
to a different database engine. Since we started improving SQLite support in Akonadi during
the year, we received many requests from users about how they can easily switch from MySQL
or PostgreSQL to SQLite without having to wipe their entire configuration and setup everything
from scratch with SQLite. With the database migrator tool, it will be very easy for users to
migrate between the supported database engines without having to re-configure and re-sync all
their accounts afterwards.

Dan's work on this tool is funded by [g10 Code GmbH](https://g10code.com).

## KMail

The email composer received some small visual changes and adopted a more
frameless look similar to other KDE applications. In addition, the composer
will directly display whether the openPGP key of a recipient exists and is
valid when encryption is enabled.

![KMail composer new look](images/kmail-composer.png)

The rust based adblocker received further work and adblock lists can be added
or removed.

![Adblock configuration with the list of adblock list](images/kmail-adblock.png)

We removed changing the charset of a message and now only support sending
emails as UTF-8. This is nowaday supported by all the other email clients. But
we still support reading emails in other encoding.

Now kmail intercepts SIGINT/SIGTERM signal. It avoids to close by error KMail
during editing.

Laurent finished to implement the "allow to reopen closed viewer" feature. This
provides a menu which allows to reopen message closed by error.

In addition a lot of bugs were fixed. This includes:
  * Fix bug 478352:  Status bar has "white box" UI artifact ([BKO#478352](https://bugs.kde.org/478352)).
  * Load on demand specific widget in viewer. Reduce memory foot.
  * Fix mailfilter agent connection with kmail (fix apply filters)

A lot of work was done to rewrite the account wizard in QML.
Accounts can now be created manually.

## KAddressBook

Bug 478636 was fixed. Postal addresses was ruined when we added address in contact. ([BKO#478636](https://bugs.kde.org/478636)).
Now KAddressbook intercepts SIGINT/SIGTERM signal. It avoids to close by error it during editing contact.

## Kleopatra

Development focussed on fixing bugs for the release of GnuPG VS-Desktop® (which includes Kleopatra) in December.
* OpenPGP keys with valid primary key and expired encryption or signing subkeys are now handled correctly ([T6788](https://dev.gnupg.org/T6788)).
* Key groups containing expired keys are now handled correctly ([T6742](https://dev.gnupg.org/T6742)).
* The currently selected certificate in the certificate list stays selected when opening the certificate details or certifying the certificate ([T6360](https://dev.gnupg.org/T6360)).
* When creating an encrypted archive fails or is aborted, then Kleopatra makes sure that no partially created archive is left over ([T6584](https://dev.gnupg.org/T6584)).
* Certificates on certain smart cards are now listed even if some of the certificates couldn't be loaded ([T6830](https://dev.gnupg.org/T6830)).
* The root of a certificate chain with two certificates is no longer shown twice ([T6807](https://dev.gnupg.org/T6807)).
* A crash when deleting a certificate that is part of a certificate chains with cycles was fixed ([T6602](https://dev.gnupg.org/T6602)).
* Kleopatra now supports the special keyserver value "none" to disable keyserver lookups ([T6761](https://dev.gnupg.org/T6761)).
* Kleopatra looks up a key for an email address via WKD (key directory provided by an email domain owner) even if keyserver lookups are disabled ([T6868](https://dev.gnupg.org/T6868)).
* Signing and encryption of large files is now much faster (especially on Windows where it was much slower than on Linux) ([T6351](https://dev.gnupg.org/T6351)).

## pim-sieve-editor

2 bugs were fixed in sieveeditor:
  * Fix BUG: 477755: Fix script name ([BKO#477755](https://bugs.kde.org/477755)).
  * Fix bug 476456: No scrollbar in simple editing mode ([BKO#476456](https://bugs.kde.org/476456)).

Now sieveeditor intercepts SIGINT/SIGTERM signal. It avoids to close by error it during editing sieve script.

## pim-data-exporter

Now it intercepts SIGINT/SIGTERM signal. It avoids to close application during import/export.
