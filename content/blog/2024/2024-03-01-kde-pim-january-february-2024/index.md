---
title: January and February in KDE PIM
author: KDE PIM Team
date: 2024-03-05T12:00:00+00:00
accountwizard:
- name: Welcome Page
  url: account-welcome.png
- name: Configuration slection
  url: account-automatic.png
- name: Manual configuration
  url: account-manual.png
---

Here's our bi-monthly update from KDE's personal information management
applications team. This report covers progress made in the months of January and February 2024.

Since [the last report](/blog/2024/2024-01-01-kde-pim-november-december-2023/), 34
people contributed approximately 1742 code changes, focusing on bugfixes and
improvements for the coming 24.02 release based on Qt6.


## Akonadi

The database migration tool to help users easily switch between different database
engines has been completed and merged. The tool can be used like this to migrate
from the current engine to SQLite, however migration between any combinations of the
supported database engines works.

```
akonadi-db-migrator --newengine sqlite
```

Unfortunately, the migrator is not available in the 24.02 megarelease, as some big fixes
came too late to be safely merged into a stable branch. It will be included in the next
feature release.

We also tried to improve handling of a race condition when syncing a very large IMAP folder,
where if user marked an email as read in KMail while the folder was syncing, the change got
reverted by the sync.

There were also some additional code improvements, test fixes and some last-minute fixes due
to changes betwene Qt5 and Qt6 that we missed.

Thank's to [g10 Code GmbH](https://g10code.com) for sponsoring part of Dan's work on Akonadi
improvements.


## KOrganizer

A long-standing issue with un-answered and declined invitations not showing in KOrganizer was fixed.
It's common when using enterprise calendar solutions (Google, EWS, etc.) that when a user is invited
into a meeting, the invitation is automatically added to their calendar. KOrganizer used to filter
those out and instead implemented special "Open Invitations" and "Declined Invitations" meta-calendars
that were supposed to show those. Unfortunately this feature never really worked properly and so
users were confused where are their meetings. This "feature" has been removed in 24.02 and KOrganizer
will show all invites without any filter. In 24.05 release, KOrganizer will also have a checkbox
in its Settings to let users to choose whether to hide declined invitations globally.


* Fixed syncing of Google contacts not working if the sync token has expired.
* Prepared for upcoming changes in Google Calendar API behavior.
* Calendar colors not matching configured colors in Google or DAV has been fixed.
* When creating a recurrence exception, KOrganizer now uses correct timezone [#481305](https://bugs.kde.org/show_bug.cgi?id=481305).
* Fixed 'Create sub-todo' action in KOrganizer being always disabled [#479351](https://bugs.kde.org/show_bug.cgi?id=479351).
* Fixed iCal resource getting stuck in a loop and consuming lots of CPU when syncing specific calendars [#384309](https://bugs.kde.org/show_bug.cgi?id=384309).

Dan's work on improving KOrganizer and calendaring was sponsored by [g10 Code GmbH](https://g10code.com).


## KMail

Big new feature is support for OAuth login for personal Outlook365 accounts via IMAP and SMTP.
The OAuth login is more reliable than standard username+password authentication and it means
that users with Two Factory Authentication for their Outlook365 accounts no longer have to set up
an app-specific password. Note that users who have account as part of their school or work
organizations should use the EWS resource, which already supports OAuth login.

The account wizard rewrite using QML is progressing and aside from some edge
cases, the automatic configuration of your account based on the Thunderbird
online configuration should work again. The UI also received some improvements:

{{< screenshots name="accountwizard" >}}

In addition a lot of bugs were fixed. This includes:

* Fixed encrpyted attachments not showing in email header even after decrypting the email.
* Fixed EWS configuration dialog asking for both an username and an email address
* Changed the default encryption of new IMAP and SMTP connections to the more secure SSL/TLS transport


## Itinerary

With the Qt 6 transition completed [Itinerary](https://apps.kde.org/itinerary) has gotten
a new journey map view, public transport data coverage for more countries and many more
improvements. See Itinerary's [bi-monthly status update](https://volkerkrause.eu/2024/02/02/kde-itinerary-december-january-2024.html) for more details.

![Itinerary showing a planned train trip on a map.](images/kde-itinerary-journey-map.jpg)


## Kleopatra

For large files the performance of encryption, decryption, signing, and signature verification has been improved
(by making gpg read and write the files directly). This is most noticeable on Windows. [T6351](https://dev.gnupg.org/T6351)

The usability was improved in a few places:
* The configuration of certificate groups is now much easier because changes are applied directly. [T6662](https://dev.gnupg.org/T6662)
* Users can now change the name of the decryption result if a file with the same name already exists. [T6851](https://dev.gnupg.org/T6851)
* The workflow of moving a key to a smart card was streamlined. [T6933](https://dev.gnupg.org/T6933)
* Kleopatra now reads certificates automatically from certain smart cards. [T6846](https://dev.gnupg.org/T6846)

Additionally, a few bugs affecting the Windows builds have been fixed:
* A wrong permission check when writing the decryption result to a network drive was fixed.
  (Technical details: Qt allows to enable expensive
   [ownership and permissions checking on NTFS file systems](https://doc.qt.io/qt-6/qfileinfo.html#ntfs-permissions).
   Unfortunately, these checks seem to give wrong results for network drives.) [T6917](https://dev.gnupg.org/T6917)
* Kleopatra now displays all icons also in dark mode. Previously, some icons, most noteably the system tray icon,
  were missing when dark mode was enabled. [T6926](https://dev.gnupg.org/T6926)

Related to Kleopatra, pinentry-qt, the small program that asks for the password to unlock your OpenPGP keys also received
some love:
* pinentry-qt can now be built for Qt 6. [T6883](https://dev.gnupg.org/T6883)
* pinentry-qt now supports external password managers (via libsecret), but only outside of KDE Plasma to avoid deadlocks
  with KWallets using GnuPG to encrypt the passwords. [T6801](https://dev.gnupg.org/T6801)
* pinentry-qt now has better support for Wayland. [T6887](https://dev.gnupg.org/T6887)
