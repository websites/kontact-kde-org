---
title: Download
layout: download
appstream: org.kde.kontact
name: Kontact
gear: true
flatpak: true
flatpak_exp: https://community.kde.org/KDE_PIM/Flatpak
menu:
  main:
    weight: 2
sources:
  - name: Git
    src_icon: /reusable-assets/git.svg
    description: >
        Kontact is spread across a multitude of repositories. Check our
        [Getting Involved](/get-involved) page for detailed
        instructions how to get all Kontact repositories.
  - name: Windows
    src_icon: /reusable-assets/windows.svg
    description: >
        We are currently working on bringing Kontact to Windows. Do you want
        to help us? Check [how to get involved](/get-involved)
        and get in touch with us!
konqi: /assets/img/konqi-mail.png
components: true
---
